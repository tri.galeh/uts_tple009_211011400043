import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'project app shoes',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ShoesList(),
    );
  }
}

class ShoesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shoes'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                'https://tse3.mm.bing.net/th?id=OIP.GlIuUj-GYrRL_G8WvZ3YagHaHw&pid=Api&P=0&h=180',
              ),
              radius: 25, // Modify the radius to make it slightly larger
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          buildShoeItem(
            title: 'Nike SB Zoom Blazer',
            subtitle: 'Mid Premium',
            price: '£8,795',
            imageUrl: 'https://www.freepnglogos.com/uploads/shoes-png/shoes-wasatch-running-3.png',
            Colors.deepPurple.shade300
          ),
          buildShoeItem(
            title: 'Nike Air Zoom Pegasus',
            subtitle: "Men's Rood Running Shoes",
            price: '£9,995',
            imageUrl: 'http://pngfolio.com/images/all_img/copy/1635221496shoes-png-image.png',
            Colors.cyan.shade100
          ),
          buildShoeItem(
            title: 'Nike ZoomX Vaporfly',
            subtitle: "Men's Road Racing Shoe",
            price: '£19,695',
            imageUrl: 'https://www.freepnglogos.com/uploads/shoes-png/shoes-wasatch-running-3.png',
            Colors.pink.shade50
          ),
          buildShoeItem(
            title: 'Nike Air Force 1 S50',
            subtitle: "Older Kids' Shoe",
            price: '£6,295',
            imageUrl: 'https://www.freepnglogos.com/uploads/shoes-png/shoes-wasatch-running-3.png',
            Colors.grey.shade200
          ),
          buildShoeItem(
            title: 'Nike Waffle One',
            subtitle: "Men's Shoes",
            price: '£8,295',
            imageUrl: 'http://pngfolio.com/images/all_img/copy/1635221496shoes-png-image.png',
            Colors.yellowAccent.shade100
          ),
        ],
      ),
    );
  }

  Widget buildShoeItem({
    required String title,
    required String subtitle,
    required String price,
    required String imageUrl,
    required Color color
  }) {
    return Card(
      color: color
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween, // Align elements to the right
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 18),
                ),
                Text(
                  subtitle,
                  style: TextStyle(fontSize: 14, color: Colors.grey),
                ),
                Text(
                  price,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(width: 16),
            Image.network(imageUrl)
          ],
        ),
      ),
    );
  }
}